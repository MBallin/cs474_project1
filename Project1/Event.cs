﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project1
{
    class Event
    {
        public enum eventType : int
        {
            local = 1,
            send = 2,
            receive = 3,
            count = 4
        };

        public eventType type;
        public int logicalClock;
        public int label;
        public string name;

        public Event(eventType newType, string newName, int newLabel, int newClock)
        {
            name = newName;
            type = newType;
            label = newLabel;
            logicalClock = newClock;
        }

        public Event(eventType newType)
        {
            name = "a";
            type = newType;
            label = 0;
            logicalClock = 0;
        }
    }
}
