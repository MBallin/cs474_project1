﻿using Project1;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CS474_Project1
{
    class Process
    {
        public string processName { get; set; } = "defaultProcess";
        public List<Event> eventList;

        public Process()
        {
            Event events = new Event(Event.eventType.local, "-", 0, 0);
            eventList = new List<Event>();
        }

        public Process(string newProcessName, params string[] events)
        {
            processName = newProcessName;
            eventList = new List<Event>();
            foreach (string e in events)
            {
                // The minus 48 is to convert the character label to an integer
                if (e.Any(char.IsDigit))
                {
                    if (e[0].ToString().ToLower().Equals("s"))
                    {
                        eventList.Add(new Event(Event.eventType.send, "s", e[1] - 48, 0));
                    }
                    else
                    {
                        eventList.Add(new Event(Event.eventType.receive, "r", e[1] - 48, 0));
                    }
                }
                else
                {
                    eventList.Add(new Event(Event.eventType.local, e[0].ToString(), 0, 0));
                }
            }
        }

        public override string ToString()
        {
            string output = this.processName + " : ";
            foreach (Event e in eventList)
            {
                if (e.type == Event.eventType.local)
                {
                    output += e.name + " ";
                }
                else
                {
                    output += e.name + e.label + " ";
                }
            }
            return output;
        }

        public string printLogicalClock()
        {
            string output = this.processName + " : ";
            foreach (Event e in eventList)
            {
                output += e.logicalClock + " ";
            }
            return output;
        }
    }
}
