﻿using Project1;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace CS474_Project1
{
    class Program
    {
        // Declare variables.
        static int maxSendEvents = 9;
        static int maxInternalEvents = 25;
        static string inputFile = "processInput.txt";
        static string path = Path.GetFullPath(Path.Combine(Environment.CurrentDirectory, @"..\..\"));
        static List<Process> processList = new List<Process>();

        // Stores the clock value for the send and receive events
        static int[] sendArray = new int[maxSendEvents];
        static int[] receiveBitmap = new int[maxSendEvents];

        static void Main(string[] args)
        {
            // Parse file 
            using (StreamReader sr = File.OpenText(path + inputFile))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    Process newProcess = new Process(line.Split(':')[0], line.Split(':')[1].Split(','));
                    processList.Add(newProcess);
                }
            }

            List<Process> generatedBoard = generateBoard(5);

            // Print solved logical clocks list
            //foreach (Process p in generatedBoard)
            //{
            //    Console.WriteLine(p.printLogicalClock());
            //}

            //// Print process list
            foreach (Process p in processList)
            {
                Console.WriteLine(p.ToString());
            }
            Console.WriteLine();

            // Solve all process clocks
            solveProcessClocks(processList);

            // Print solved logical clocks list
            foreach (Process p in processList)
            {
                Console.WriteLine(p.printLogicalClock());
            }
            Console.ReadLine();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static bool solveProcessClocks(List<Process> processList)
        {
            bool done = false;
            while(!done)
            {
                foreach(Process p in processList)
                {
                    solveSingleProcessClock(p);
                }
                done = true;
                foreach (Process p in processList)
                {
                    foreach(Event e in p.eventList)
                    {
                        if (e.logicalClock == 0)
                        {
                            done = false;
                            break;
                        }
                    }
                    if(!done)
                    {
                        break;
                    }
                }
            }
            return done;
        }

        /// <summary>
        /// Solves as much of a local process as it can.
        /// </summary>
        /// <param name="process"></param>
        /// <returns></returns>
        public static void solveSingleProcessClock(Process process)
        {
            // Loop through all events
            for(int i = 0; i < process.eventList.Count; i++)
            {
                // If we have yet to give a clock value to an event, do so
                if (process.eventList[i].logicalClock == 0)
                {
                    switch (process.eventList[i].type)
                    {
                        case Event.eventType.local:
                            if (i == 0)
                            {
                                process.eventList[i].logicalClock = 1;
                            }
                            else if (process.eventList[i - 1].logicalClock != 0)
                            {
                                process.eventList[i].logicalClock = process.eventList[i - 1].logicalClock + 1;
                            }
                            break;
                        case Event.eventType.receive:
                            bool breakEarly = false;
                            if (sendArray[process.eventList[i].label - 1] != 0)
                            {
                                // Check to make sure there is no receive even prior to this one that has not been solved.
                                for (int j = 0; j < i; j++)
                                {
                                    if(process.eventList[j].type == Event.eventType.receive &&
                                        sendArray[process.eventList[j].label - 1] == 0)
                                    {
                                        breakEarly = true;
                                    }
                                }
                                if(!breakEarly)
                                {
                                    if (i == 0)
                                    {
                                        process.eventList[i].logicalClock = sendArray[process.eventList[i].label - 1] + 1;
                                    }
                                    else
                                    {
                                        process.eventList[i].logicalClock = Math.Max(sendArray[process.eventList[i].label - 1], process.eventList[i - 1].logicalClock) + 1;
                                    }
                                }

                            }
                            break;
                        case Event.eventType.send:
                            if (i == 0)
                            {
                                process.eventList[i].logicalClock = 1;
                            }
                            else if (process.eventList[i - 1].logicalClock != 0)
                            {
                                process.eventList[i].logicalClock = process.eventList[i - 1].logicalClock + 1;
                            }
                            sendArray[process.eventList[i].label - 1] = process.eventList[i].logicalClock;
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        public static List<Process> generateBoard(int numProcesses)
        {
            List<Process> board = new List<Process>();
            for(int i = 0; i < numProcesses; i++)
            {
                board.Add(new Process());
                board[i].processName = "p" + i;
            }

            List<int> sendLabels = new List<int>(UniqueRandom(9, 1, 9));
            Thread.Sleep(100);
            List<int> receiveLabels = new List<int>(UniqueRandom(9, 1, 9));

            foreach (Process p in board)
            {
                Thread.Sleep(100);
                for (int i = 0; i < 25; i++)
                {
                    p.eventList.Add(new Event(Event.eventType.local));
                }
                 
                List<int> sendReceiveIndicies = UniqueRandom(4, 0, 24);
                p.eventList[sendReceiveIndicies[0]] = new Event(Event.eventType.send, "s", sendLabels[0], 0);
                sendReceiveIndicies.RemoveAt(0);
                sendLabels.RemoveAt(0);
                p.eventList[sendReceiveIndicies[0]] = new Event(Event.eventType.receive, "r", receiveLabels[0], 0);
                sendReceiveIndicies.RemoveAt(0);
                receiveLabels.RemoveAt(0);
                if(sendLabels.Count > 0 && receiveLabels.Count > 0)
                {
                    p.eventList[sendReceiveIndicies[0]] = new Event(Event.eventType.send, "s", sendLabels[0], 0);
                    sendReceiveIndicies.RemoveAt(0);
                    sendLabels.RemoveAt(0);
                    p.eventList[sendReceiveIndicies[0]] = new Event(Event.eventType.receive, "r", receiveLabels[0], 0);
                    sendReceiveIndicies.RemoveAt(0);
                    receiveLabels.RemoveAt(0);
                }
            }
            return new List<Process>(board);
        }

        /// <summary>
        /// Returns some amount of numbers, between min and max inclusive, once in a random sequence.
        /// </summary>
        public static List<int> UniqueRandom(int numTerms, int minInclusive, int maxInclusive)
        {
            List<int> candidates = new List<int>();
            Random rng = new Random();
            while (candidates.Count < numTerms)
            {
                int num = rng.Next(minInclusive, maxInclusive + 1);
                if (candidates.IndexOf(num) == -1)
                {
                    candidates.Add(num);
                }
            }
            return candidates;
        }
    }
}
